// scroll bg navbar da trasparente a solido
const navEl = document.querySelector('.navbar');

window.addEventListener('scroll', () => {
    if (window.scrollY >= 56) {
        navEl.classList.add('navbar-scrolled');
    }else if (window.scrollY <= 56) {
        navEl.classList.remove('navbar-scrolled');
    }
})



fetch("cocktails.json")
.then(response => response.json())
.then(dati => {
    
// Salvare i dati
const appState = dati.cocktails;



// preso l'id dove appendere i dati 
let totalCocktails = document.querySelector('#totalCocktails');
// restituisce la lunghezza dell'oggetto
totalCocktails.textContent = appState.length

// anima il numero 
animateValue(totalCocktails, 0, appState.length, 15000);

// funzione per animare il numero 
function animateValue(element, start, end, duration) {
    let range = end - start;
    let current = start;
    let increment = end > start ? 1 : -1;
    let stepTime = Math.abs(Math.floor(duration / range));
    let timer = setInterval(() => {
        current += increment;
        element.textContent = current;
        if (current == end) {
            clearInterval(timer);
        }
    }, stepTime);
}




// preso l'id dove appendere i dati
let totalIngredients = document.querySelector('#totalIngredients');
// posto dove salvare tutti gli ingredienti
let ingr = [];
// ciclare cocktail che a sua volta ciclare gli ingredienti 
appState.forEach(cocktail => {
    cocktail.ingredients.forEach(ingredient => {
        ingr.push(ingredient.name)
    })
});
// restituisce il totale di ingredienti, new set rimuove i doppioni, essendo che set è un TIPO, dobbiamo convertirlo in Array
totalIngredients.textContent = Array.from(new Set(ingr)).length

// anima il numero 
animateValue(totalIngredients , 0, Array.from(new Set(ingr)).length, 14000);

// funzione per animare il numero 
function animateValue(element, start, end, duration) {
    let range = end - start;
    let current = start;
    let increment = end > start ? 1 : -1;
    let stepTime = Math.abs(Math.floor(duration / range));
    let timer = setInterval(() => {
        current += increment;
        element.textContent = current;
        if (current == end) {
            clearInterval(timer);
        }
    }, stepTime);
}







// preso l'id dove appendere i dati
let totalPhotos = document.querySelector('#totalPhotos');
// contatore
let foto = 0;
// ciclo per aggiungere tutte le foto 
appState.forEach(cocktail => {
    foto += cocktail.photos.length;
})
// restituisce il totale delle foto 
totalPhotos.textContent = foto





// anima il numero 
animateValue(totalPhotos , 0, foto, 15000);

// funzione per animare il numero 
function animateValue(element, start, end, duration) {
    let range = end - start;
    let current = start;
    let increment = end > start ? 1 : -1;
    let stepTime = Math.abs(Math.floor(duration / range));
    let timer = setInterval(() => {
        current += increment;
        element.textContent = current;
        if (current == end) {
            clearInterval(timer);
        }
    }, stepTime);
}




//card con dati, creata una copia e restituita in forma di array delle diverse chiavi con il map, con il sort ho deciso di ordinarli per la posizione 3 dal più grande al più piccolo, con slice mi prende dalla posizione 0 a 3 escluso 4 
let contentCard = appState.map((cocktail) => {
    return [cocktail.name, cocktail.photos[0], cocktail.description, cocktail.ranking, cocktail.id]
}).sort((a,b) => b[3] - a[3]).slice(0,3)
// preso l'id dove appendere i dati
let cocktailCardWrapper = document.querySelector('#cocktailCardWrapper')
// cicla ogni card 
contentCard.forEach((el) => {
    // per ogni elemento creami un div
    let div = document.createElement('div')
    // crea la classe da dare al div
    div.classList.add("col-12", "col-md-4", "mb-4", "item") 
    // crea la card
    div.innerHTML = ` 

                <div class="card my-4 border rounded-2 bg-transparent h-100">
                        <img src="${el[1]}" class="custom-img-style">
                    <div class="card-body">
                            <h5 class="card-title p-2 fw-bold prata text-uppercase second-color">${el[0]}</h5>
                             <small class="card-text text-light d-flex justify-content-end me-2 mb-2 fst-italic">${el[3]}/5</small>
                        <p class="card-text fourth-color fs-5">${el[2]}</p>
                    </div>
<button class="btn btn-outline-light text-uppercase mb-0" onclick="window.location.href = 'detail.html?id=${el[4]}'">dettaglio</button>
                </div>`  
      
// appendere sull'HTML
cocktailCardWrapper.appendChild(div)
})



// map per prendere le foto nell'array
const gallery = appState.map((cocktail) => {
    return[cocktail.photos[0], cocktail.photos[1], cocktail.photos[2]];
})
// preso l'id dove appendere i dati
let cocktailPhotoWrapper = document.querySelector('#cocktailPhotoWrapper')
// cicla ogni card per appendere le foto in un carousel
gallery.forEach((el) => {
    // per ogni elemento creami un div
    let div = document.createElement('div')
    // crea la classe da dare al div
    div.classList.add("carousel-item")
    // crea la card
    div.innerHTML = ` 
                <div class="d-flex justify-content-evenly">
                
                  <div class="card mx-5 bg-transparent border-0 image-wrapper">
                    <img src="${el[0]}" class="custom-img-style" alt="...">
                  </div>
                  
                  <div class="card mx-5 bg-transparent border-0 image-wrapper">
                    <img src="${el[1]}" class="custom-img-style" alt="...">
                  </div>

                  <div class="card mx-5 bg-transparent border-0 image-wrapper">
                  <img src="${el[2]}" class="custom-img-style" alt="...">
                </div>
                </div>`
// appendere sull'HTML
cocktailPhotoWrapper.appendChild(div)
})






//passiamo l'id che permette che al click entra nella pagina dettaglio 
let urlParams = new URLSearchParams(window.location.search);
let id = window.location.search.split("=")[1]








})