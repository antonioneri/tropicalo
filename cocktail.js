const navEl = document.querySelector('.navbar');

window.addEventListener('scroll', () => {
    if (window.scrollY >= 56) {
        navEl.classList.add('navbar-scrolled');
    }else if (window.scrollY <= 56) {
        navEl.classList.remove('navbar-scrolled');
    }
})

fetch('cocktails.json')
.then(response => response.json())
.then(dati => {

    const allCocktail = dati.cocktails;
    console.log(allCocktail);

// placeholder nella barra di ricerca
let searchBar = document.querySelector('#searchBar');
searchBar.setAttribute('placeholder', `🔎 search  between  ${allCocktail.length}  cocktails...`)



let cardWrapper = document.querySelector('#cardWrapper');

function showCocktail(array) {
    cardWrapper.innerHTML = "";
array.forEach((el) => {
    let div = document.createElement('div')
    div.classList.add("col-12", "col-md-4", "mb-5")
    div.innerHTML = `
                    <div class="card my-4 border shadow rounded-2 bg-transparent h-100">
                        <img src="${el.photos[1]}" class="custom-img-style">
                            <div class="card-body">
                                <h5 class="card-title p-2 fw-bold prata text-uppercase second-color">${el.name}</h5>
                                <h6 class="card-text text-light d-flex justify-content-end me-2 mb-2 baloo">${el.ranking}</h6>
                                <p class="card-text fourth-color fs-5">${el.description}</p>
                                <hr class="text-white w-75 mx-auto">
                                <p class="p-1 fw-bold prata text-capitalize second-color">ingredienti</p>
                                <span class="fourth-color">${el.ingredients.map(obj => { return obj.name })}</span>
                            </div>
    <button class="btn btn-outline-light text-uppercase mb-0" onclick="window.location.href = 'detail.html?id=${el.id}'">
    dettaglio</button>
</div>
    `
    cardWrapper.appendChild(div)
});
}

showCocktail(allCocktail)

//passiamo l'id che permette che al click entra nella pagina dettaglio 
let urlParams = new URLSearchParams(window.location.search);
let id = window.location.search.split("=")[1]



//ingredienti
let ingredientsWrapper = document.querySelector('#ingredientsWrapper');

// generare l'array di tutti gli ingredienti univoci
let ingredient = Array.from(new Set(allCocktail.flatMap(cocktail => cocktail.ingredients.map(ingredient => ingredient.name))));

//settato tutti gli ingredienti nella select
ingredient.forEach(ingredient => {
    let option = document.createElement('option')
    option.value = option.id = ingredient;
    option.dataset.ingredient = "ingredient"
    option.innerHTML = `${ingredient}`
    ingredientsWrapper.appendChild(option)
})


//selezionare ingredienti dalla select 
let select = document.querySelector('#ingredientsWrapper')

// al change restituisce le card in base all'ingrediente selezionato
select.addEventListener('change', () => {
    let selectedValue = select.value;
    if (selectedValue === "") {
        showCocktail(allCocktail);
      } else {
        showCocktail(filterByIngredients(allCocktail, selectedValue));
      }
  });

// filtra per valore
function filterByIngredients(array, value) {
    return array.filter(el => {
      return el.ingredients.some(ingredient => ingredient.name === value);
    });
  }

  

// search per nome
function filterByWord(array, word) {
    return array.filter((el) => {
        return el.name.match(new RegExp(word, "gi"))
    })
}
// l'evento all'input restituisce le card con i valori inseriti nella search
searchBar.addEventListener('input', () => {
    showCocktail(filterByWord(allCocktail, searchBar.value))
})



// ordina per 
const sel = document.getElementById('sel');

//dal più grande al più piccolo 
function sortingByUp(array) {
    return array.sort((a,b) => b.ranking - a.ranking)
}

//dal più piccolo al più grande 
function sortingByDown(array) {
    return array.sort((a,b) => a.ranking - b.ranking)
}

//resetta 
function resetOrder(array) {
    return array.sort((a,b) => a.id - b.id);
  }

// filtra per ranking
sel.addEventListener('change', () => {
    let val = sel.value;
    if (val === "up") {
        showCocktail(sortingByUp(allCocktail))
    } else if (val === "down") {
        showCocktail(sortingByDown(allCocktail))
    } else {
        showCocktail(resetOrder(allCocktail))
    }
})



})