const navEl = document.querySelector('.navbar');

window.addEventListener('scroll', () => {
    if (window.scrollY >= 56) {
        navEl.classList.add('navbar-scrolled');
    }else if (window.scrollY <= 56) {
        navEl.classList.remove('navbar-scrolled');
    }
})

function validateForm() {
    let name =  document.getElementById('name').value;
    if (name == "") {
        document.querySelector('.status').innerHTML = "Inserisci Nome";
        return false;
    }
    let email =  document.getElementById('email').value;
    if (email == "") {
        document.querySelector('.status').innerHTML = "Inserisci Email";
        return false;
    } else {
        let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(!re.test(email)){
            document.querySelector('.status').innerHTML = "Email non corretta";
            return false;
        }
    }
    let subject =  document.getElementById('subject').value;
    if (subject == "") {
        document.querySelector('.status').innerHTML = "Inserisci Oggetto";
        return false;
    }
    let message =  document.getElementById('message').value;
    if (message == "") {
        document.querySelector('.status').innerHTML = "Inserisci Messaggio";
        return false;
    }
    let done = document.querySelector('.status')
    done.classList.remove('alert-danger')
    done.classList.add('alert-success')
    done.innerHTML = "Inviato";
    setTimeout(() => {
        location.reload();
      }, 2000);
  }