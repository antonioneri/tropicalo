const navEl = document.querySelector('.navbar');

window.addEventListener('scroll', () => {
    if (window.scrollY >= 56) {
        navEl.classList.add('navbar-scrolled');
    }else if (window.scrollY <= 56) {
        navEl.classList.remove('navbar-scrolled');
    }
})


fetch("cocktails.json")
.then(response => response.json())
.then(dati => {


const drinks = dati.cocktails;

//card con dati, creata una copia e restituita in forma di array delle diverse chiavi con il map, con il sort ho deciso di ordinarli per la posizione 3 dal più grande al più piccolo, con slice mi prende dalla posizione 0 a 3 escluso 4 
let contentCard = drinks.filter((cocktail) => {
    return cocktail.anno !== undefined;
}).map((cocktail) => {
    return [cocktail.name, cocktail.photos[0], cocktail.description, cocktail.anno, cocktail.id];
}).sort((a,b) => a[3] - b[3]);
// preso l'id dove appendere i dati
let cocktailCardWrapper = document.querySelector('#cocktailCardWrapper')
// cicla ogni card 
contentCard.forEach((el) => {
    // per ogni elemento creami un div
    let div = document.createElement('div')
    // crea la classe da dare al div
    div.classList.add("col-12", "col-md-4", "mb-4", "item") 
    // crea la card
    div.innerHTML = ` 

                <div class="card old my-4 border rounded-2 bg-transparent h-100">
                        <img src="${el[1]}" class="custom-img-style">
                    <div class="card-body">
                            <h5 class="card-title p-2 fw-bold prata text-uppercase second-color">${el[0]}</h5>
                             <small class="card-text text-light d-flex justify-content-end me-2 mb-2 fst-italic">${el[3]}</small>
                        <p class="card-text fourth-color fs-5">${el[2]}</p>
                    </div>
<button class="btn btn-outline-light text-uppercase mb-0" onclick="window.location.href = 'detail.html?id=${el[4]}'">dettaglio</button>
                </div>`  
      
// appendere sull'HTML
cocktailCardWrapper.appendChild(div)
})

})