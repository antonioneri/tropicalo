const navEl = document.querySelector('.navbar');

window.addEventListener('scroll', () => {
    if (window.scrollY >= 45) {
        navEl.classList.add('navbar-scrolled');
    }else if (window.scrollY <= 45) {
        navEl.classList.remove('navbar-scrolled');
    }
})

// converte l'id ricevuto in stringa id=1 in int
let id = window.location.search.split("=")[1]

fetch("cocktails.json")
.then(response => response.json())
.then(dati => {
    const data = dati.cocktails.find(x=>{if(x.id==id)return x})
    
// appende il nome
document.querySelector('#cocktailName').innerHTML = data.name

//cicla gli ingredienti
const cocktailIngredient = document.querySelector('#cocktailIngredient');
 data.ingredients.forEach(ingredient => {
    const li = document.createElement("li");
    li.classList.add("text-center");
    li.textContent = ingredient.name;
    cocktailIngredient.appendChild(li);
});

//cicla le quantità
const cocktailQuantity = document.querySelector('#cocktailQuantity');
data.ingredients.forEach(ingredient => {
    const li = document.createElement('li');
    li.classList.add('text-center');
    li.textContent = ingredient.quantities;
    cocktailQuantity.appendChild(li);
})

// appende la gradazione
document.querySelector('#cocktailGradation').innerHTML = data.gradations

//appende il ranking
document.querySelector('#cocktailRanking').innerHTML = data.ranking

//appende la preparazione
document.querySelector('#cocktailPreparation').innerHTML = data.preparation

//appende la descrizione
document.querySelector('#cocktailDescription').innerHTML = data.description

//appende la storia
document.querySelector('#cocktailHistory').innerHTML = data.history

//appende le foto
let cocktailImage = document.querySelector('#cocktailImage')
let cocktailImage1 = document.querySelector('#cocktailImage1')
let cocktailImage2 = document.querySelector('#cocktailImage2')


let images = data.photos[0];
let images1 = data.photos[1];
let images2 = data.photos[2];

cocktailImage.src = images
cocktailImage1.src = images1
cocktailImage2.src = images2





})

